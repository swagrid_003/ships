FROM golang:1.18.0-alpine3.15 as base

WORKDIR /usr/src/app

RUN apk add --no-cache ca-certificates gcc git libc-dev \
  && go install github.com/cespare/reflex@latest \
  && go install github.com/kyoh86/richgo@latest

ENV GO111MODULE=on
ENV XDG_CACHE_HOME=/tmp/.cache

FROM base as builder
COPY . .

# Build and install a static binary, stripping DWARF debugging information and
# preventing the generation of the Go symbol table.
RUN go mod download
RUN GOOS=linux GOARCH=amd64 go install -a -ldflags '-w -s -linkmode external -extldflags "-static"' ./cmd/ports/main.go

FROM scratch

COPY --from=builder /go/bin/main /
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

ENTRYPOINT ["/main"]
