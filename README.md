# Ports Test

## Installation and Testing

Assumes the reader already has an up to date installation of Go on their
machine.

- Tools are kept separate to build in tools.go, run `make install-tools` to install the contents of tools.go.
- Run `make generate-mocks` to use mockgen to generate mocks with go generate.
- Run `make test-watch` to start a hot reloading container that continually
  runs the unit tests. Change any go file, and the tests re-run.
- Run `make run` to start a hot reloading server.

## General Notes on the Solution
The following is my attempt at the challenge. I adhered as best I could to the
two hour time limit, coming in at about 2h15mins (not including this readme).
Some general notes on my solution.

- I've included the layers/packages I normally would in some form.
  - entities/services cover the business logic.
  - repositories are for data. Can be external APIs, a file, a database etc.
  - entrypoints are program triggers. rest API, gRPC, a CLI etc.

- I've tried to demonstrate the decoupling of concerns as I normally would
  through having my "business logic" in the services package rely on interfaces
  fulfilled through the repositories/jsonfile package. The dependencies are
  inverted, such that the entities/services never depend on the
  entrypoints/repositories, allowing these things to be swapped with comparative
  ease. The "flow" through the program would in a general sense be.
  `entrypoints -> services -> repositories`.
  Injection in main/under test allows them to be completely decoupled.

- The assignment left some room for interpretation, so I chose to do a single
  "upsert" service for the ports. I thought this would make it more interesting
  as the logic has to consume a few interfaces.
- I know the assignment doesn't mention http but it _does_ mention
  demonstrating clean signal handling, so I shoved in a quick rest layer to show
  this, and also demonstrate how I normally test handlers.

## Next Steps

Given more time, I would:

- Look to the port storage layer. I'd originally planned to have a cursor based
  system that looked through the file without reading it into memory, or to spin
  up a key-value store like redis and feed the json in, but just ran out of time.
- Add more tests.
- Introduce more comprehensive errors/look at sentinels to provide more error
  context
