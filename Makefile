test-watch:
	docker-compose up ports-service-test

run:
	docker-compose up ports-service

generate-mocks:
	go generate ./...

lint:
	staticcheck -f stylish ./...

download:
	@echo Download go.mod dependencies
	@go mod download

install-tools: download
	@echo Installing tools from tools.go
	@cat tools.go | grep _ | awk -F'"' '{print $$2}' | xargs -tI % go install %
