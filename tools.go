//go:build tools
// +build tools

package tools

import (
	_ "github.com/golang/mock/mockgen"
	_ "honnef.co/go/tools/cmd/staticcheck"
)
