package jsonfile

import (
	"context"

	"github.com/CharlesWinter/ships/internal/entities"
)

// Repository is a struct with methods for i/o on that big JSON file that came
// with the test. As I won't have time to implement a robust scanning/pointer
// mechanism, nor a database and I'm told the file is too big to load into memory,
// the methods here serve just as a placeholder
type Repository struct{}

func NewRepository() (*Repository, error) {
	return &Repository{}, nil
}

func (r *Repository) PortExists(ctx context.Context, unLoc entities.UnLoc) (bool, error) {
	panic("not implemented")
}

func (r *Repository) CreatePort(ctx context.Context, port entities.Port) error {
	panic("not implemented")
}

func (r *Repository) UpdatePort(ctx context.Context, port entities.Port) error {
	panic("not implemented")
}
