package entities

import "time"

// No idea what regions should be, as it's an array it's left open to
// interpretation, let's leave it out for now.
type Location struct {
	City        string
	Country     string
	Coordinates Coordinates
	Province    string
	// It can be the repos responsibility to translate, but we want our business
	// logic to be as true a representation of the domain as possible.
	Timezone time.Location
}

type Coordinates struct {
	Latitude  float64
	Longitude float64
}
