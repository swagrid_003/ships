package entities

// Port defines our business logic entity for a singular port, it's used in the
// services, and returned from the repositories to those services.
type Port struct {
	Name  string
	UnLoc UnLoc
	Code  string
	// no need to constrain ourselves to the initial JSON here. Let's model the
	// domain how we want.
	Location Location
}
