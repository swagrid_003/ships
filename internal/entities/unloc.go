package entities

import "errors"

// UnLoc represents the United Nations Code for Trade and Transport Locations id
// for the port, which looks to basically be a unique id for a station or port.
// It's not clear why this is an array in the JSON, but let's not translate that
// to the BL
type UnLoc string

// A quick google shows an unloc should have 5 characters (6 in the US as
// they've run out of locations!). Clear in prod more would be done here
func (u UnLoc) Valid() error {
	if len(u) > 6 {
		return errors.New("unloc must have less than 7 characters")
	}
	return nil
}
