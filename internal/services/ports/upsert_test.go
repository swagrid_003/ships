package ports_test

import (
	context "context"
	"testing"

	entities "github.com/CharlesWinter/ships/internal/entities"
	"github.com/CharlesWinter/ships/internal/services/ports"
	gomock "github.com/golang/mock/gomock"
)

func TestUpsertPort(t *testing.T) {
	// Likely move this one to the entities
	t.Run("it creates a port if the port doesn't already exist", func(t *testing.T) {
		var (
			unloc    = entities.UnLoc("ABCDE")
			ctrl     = gomock.NewController(t)
			testPort = entities.Port{
				Name:  "some.name",
				UnLoc: unloc,
				Code:  "some.code",
				Location: entities.Location{
					City:    "some.city",
					Country: "some.country",
					Coordinates: entities.Coordinates{
						Latitude:  12.34,
						Longitude: 56.67,
					},
				},
			}
		)

		mockPortExists := NewMockPortExistsChecker(ctrl)
		mockPortExists.EXPECT().PortExists(gomock.Any(), unloc).Return(false, nil)

		mockPortCreator := NewMockPortCreator(ctrl)
		mockPortCreator.EXPECT().CreatePort(gomock.Any(), testPort).Return(nil)

		uc, _ := ports.NewPortDomainService(ports.PortDomainServiceConfig{
			PortExistsChecker: mockPortExists,
			PortCreator:       mockPortCreator,
		})

		if err := uc.UpsertPort(context.Background(), testPort); err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
	})
	t.Skip("it returns error if the unloc is invalid")
	t.Skip("it returns error if the port exists check fails")
	t.Skip("it returns error if the port creation fails")
	t.Skip("it updates a port if the port already exists")
	t.Skip("it errors if the port update fails")
}
