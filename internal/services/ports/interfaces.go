package ports

import (
	"context"

	"github.com/CharlesWinter/ships/internal/entities"
)

//go:generate mockgen -destination=mocks_test.go -package=ports_test -source=interfaces.go

type PortExistsChecker interface {
	PortExists(ctx context.Context, unLoc entities.UnLoc) (bool, error)
}

type PortCreator interface {
	CreatePort(ctx context.Context, port entities.Port) error
}

type PortUpdater interface {
	UpdatePort(ctx context.Context, port entities.Port) error
}
