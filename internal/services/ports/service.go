package ports

import "errors"

// PortDomainService is responsibile for business logic related to ports.
// (note: I wouldn't normally have the stutter in the name, but the example
// seemed very specific about the service name so I kept it in!)
type PortDomainService struct {
	portExistsChecker PortExistsChecker
	portCreator       PortCreator
	portUpdater       PortUpdater
}

type PortDomainServiceConfig struct {
	PortExistsChecker PortExistsChecker
	PortCreator       PortCreator
	PortUpdater       PortUpdater
}

func NewPortDomainService(cfg PortDomainServiceConfig) (*PortDomainService, error) {
	if cfg.PortExistsChecker == nil {
		return nil, errors.New("nil interface")
	}

	// etc, etc...

	return &PortDomainService{
		portExistsChecker: cfg.PortExistsChecker,
		portCreator:       cfg.PortCreator,
		portUpdater:       cfg.PortUpdater,
	}, nil
}
