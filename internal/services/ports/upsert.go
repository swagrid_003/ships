package ports

import (
	"context"

	"github.com/CharlesWinter/ships/internal/entities"
)

// Upsert port checks if the passed in port already exists (using the unloc). If
// it does it updates its info, if not it inserts a new record.
// A quick google tells us the UNLOC is a suitable unique ID for a port
// https://en.wikipedia.org/wiki/UN/LOCODE, so for the sake of this exercise it
// can serve as our PK.
// Note: The exercise was unclear as to whether these should be seperate
// usecases (create/update). I've done an upsert as it makes the business logic
// more interesting!
func (pds *PortDomainService) UpsertPort(ctx context.Context, port entities.Port) error {
	if err := port.UnLoc.Valid(); err != nil {
		return err
	}

	exists, err := pds.portExistsChecker.PortExists(ctx, port.UnLoc)
	if err != nil {
		return err
	}

	if !exists {
		return pds.portCreator.CreatePort(ctx, port)
	}
	return pds.portUpdater.UpdatePort(ctx, port)
}
