package rest

import (
	"net/http"

	"github.com/gorilla/mux"
)

// We can hang other bits here like the logger, sentry etc to be injected
type RouterConfig struct {
	PortUpserter PortUpserter
	Logger       any
}

type Handlers struct {
	PortUpserter PortUpserter
}

func NewRouter(cfg RouterConfig) http.Handler {
	router := mux.NewRouter()

	handlers := Handlers{
		PortUpserter: cfg.PortUpserter,
	}

	router.HandleFunc("/port", handlers.UpsertPort).Methods("POST")
	return router
}
