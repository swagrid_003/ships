package rest_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/CharlesWinter/ships/internal/entities"
	"github.com/CharlesWinter/ships/internal/entrypoints/rest"
	gomock "github.com/golang/mock/gomock"
)

func TestUpsertPort(t *testing.T) {
	t.Run("calls the business logic to upsert a port", func(t *testing.T) {
		timezone := "Asia/Dubai"
		location, _ := time.LoadLocation(timezone)
		var (
			unloc     = "ABCDE"
			name      = "some.name"
			code      = "some.code"
			city      = "some.city"
			country   = "some.country"
			latitude  = 12.34
			longitude = 56.78
			wantPort  = entities.Port{
				Name:  name,
				UnLoc: entities.UnLoc(unloc),
				Code:  code,
				Location: entities.Location{
					City:    city,
					Country: country,
					Coordinates: entities.Coordinates{
						Latitude:  latitude,
						Longitude: longitude,
					},
					Timezone: *location,
				},
			}
			payloadPort = rest.ApiPort{
				Name:        name,
				Coordinates: []float64{latitude, longitude},
				City:        city,
				Country:     country,
				TimeZone:    "Asia/Dubai",
				UnLoc:       unloc,
				Code:        code,
			}
		)
		ctrl := gomock.NewController(t)
		mockPortUpserter := NewMockPortUpserter(ctrl)
		mockPortUpserter.EXPECT().UpsertPort(gomock.Any(), wantPort).Return(nil)

		router := rest.NewRouter(rest.RouterConfig{
			PortUpserter: mockPortUpserter,
		})

		body, _ := json.Marshal(payloadPort)
		r := httptest.NewRequest("POST", "/port", bytes.NewReader(body))
		w := httptest.NewRecorder()

		router.ServeHTTP(w, r)

		// now we would do some assertions but I've ran out of time!
		var got string
		json.Unmarshal(w.Body.Bytes(), &got)
		if w.Code != http.StatusOK {
			t.Fatalf("got bad code %d with msg %s", w.Code, got)
		}
	})
}
