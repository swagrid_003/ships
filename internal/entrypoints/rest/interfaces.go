package rest

import (
	"context"

	"github.com/CharlesWinter/ships/internal/entities"
)

//go:generate mockgen -destination=mocks_test.go -package=rest_test -source=interfaces.go

type PortUpserter interface {
	UpsertPort(ctx context.Context, port entities.Port) error
}
