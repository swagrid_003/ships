package rest

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/CharlesWinter/ships/internal/entities"
)

// it's important not to unmarshal straight into the entity, or we tie the
// program API to the business logic directly. Let's maintain this abstraction.
// For the sake of the exercise, lets keep the JSON schema the same as the
// ports.json
type ApiPort struct {
	Name        string    `json:"name"`
	Coordinates []float64 `json:"coordinates"`
	City        string    `json:"city"`
	Province    string    `json:"province"`
	Country     string    `json:"country"`
	Regions     string    `json:"regions"`
	TimeZone    string    `json:"timezone"`
	UnLoc       string    `json:"unloc"`
	Code        string    `json:"code"`
}

func (h *Handlers) UpsertPort(w http.ResponseWriter, r *http.Request) {
	bBody, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "couldn't parse request body", http.StatusBadRequest)
		return
	}

	var apiPort ApiPort
	if err := json.Unmarshal(bBody, &apiPort); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	port, err := convAPIPortToEntity(apiPort)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := h.PortUpserter.UpsertPort(r.Context(), port); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func convAPIPortToEntity(i ApiPort) (entities.Port, error) {
	// note: of course more payload validation can be added here. I've just not
	// got time!

	// is the timezone IANA compliant?
	if i.TimeZone == "" {
		return entities.Port{}, fmt.Errorf("timezone cannot be empty")
	}

	timezone, err := time.LoadLocation(i.TimeZone)
	if err != nil {
		return entities.Port{}, err
	}

	// Best to be safe as we're going to dereference below.
	if timezone == nil {
		return entities.Port{}, fmt.Errorf("nil timezone returned for parameter: %s", i.TimeZone)
	}

	// we take coordinates as an array as a fun example, are there only two?
	if len(i.Coordinates) != 2 {
		return entities.Port{}, errors.New("must provide exactly two coordinates")
	}

	return entities.Port{
		Name:  i.Name,
		UnLoc: entities.UnLoc(i.UnLoc),
		Code:  i.Code,
		Location: entities.Location{
			City:    i.City,
			Country: i.Country,
			Coordinates: entities.Coordinates{
				Latitude:  i.Coordinates[0],
				Longitude: i.Coordinates[1],
			},
			Province: i.Province,

			Timezone: *timezone,
		},
	}, nil
}
