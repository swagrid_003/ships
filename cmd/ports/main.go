package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/CharlesWinter/ships/internal/entrypoints/rest"
	"github.com/CharlesWinter/ships/internal/repositories/jsonfile"
	"github.com/CharlesWinter/ships/internal/services/ports"
)

func main() {
	jsonFileRepo, err := jsonfile.NewRepository()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	ports, err := ports.NewPortDomainService(ports.PortDomainServiceConfig{
		PortExistsChecker: jsonFileRepo,
		PortCreator:       jsonFileRepo,
		PortUpdater:       jsonFileRepo,
	})
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	router := rest.NewRouter(rest.RouterConfig{
		PortUpserter: ports,
	})

	// obviously with more time this would be passed in through the environment
	port := 8080
	server := http.Server{
		Addr:        fmt.Sprintf(":%d", port),
		Handler:     router,
		ReadTimeout: time.Duration(time.Second * 5),
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Println("server run", err)
			os.Exit(1)
		}
	}()
	log.Printf("starting server on port %d\n", port)

	<-done

	log.Println("server stopped")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	if err := server.Shutdown(ctx); err != nil {
		log.Println(err)
		os.Exit(1)
	}
	log.Print("server exited OK")
}
